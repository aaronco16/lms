#!/usr/bin/env bash

declare -x POD="stable-koala-lms"
declare -x CONTAINER="stable"

podman stop -t=1 "${CONTAINER}"
podman rm "${CONTAINER}"

podman pod rm "${POD}"
podman pod create --name="${POD}" --share net -p 8080:8080

podman create --name="${CONTAINER}" \
              --pod="${POD}" \
              -e DEBUG=1 \
              --add-host "${CONTAINER}":127.0.0.1 \
              --add-host "${POD}":127.0.0.1 \
              --hostname "${POD}" \
              registry.gitlab.com/koala-lms/lms:latest

podman start "${CONTAINER}"
